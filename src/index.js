import 'babel-polyfill'

import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:tile-queuer:index')


import * as _ from 'lodash'
import * as redis from 'then-redis'

import * as lines from '@kaku/common/lines'


const redisOptions = {
  host: process.env.KAKU_REDIS_ADDRESS,
  port: process.env.KAKU_REDIS_PORT,
  password: process.env.KAKU_REDIS_PASSWORD
}

const redisClient = redis.createClient(redisOptions)
const redisClientInc = redis.createClient(redisOptions)
const redisClientSub = redis.createClient(redisOptions)


redisClientSub.subscribe('tiles.done-channel')
redisClientSub.on('message', (channel, message) => {
  const data = JSON.parse(message)

  redisClient.multi()
  redisClient.srem(`tiles.paint.${data.paint.id}`,
    JSON.stringify(data.position))
  redisClient.scard(`tiles.paint.${data.paint.id}`)
  redisClient.exec()
    .then((redisData) => {
      const tilesLeft = redisData[1]

      if (tilesLeft === 0) {
        redisClient.smembers(`tiles.paint-total.${data.paint.id}`)
          .then((redisData) => {
            redisClient.publish('tiles.paint-done-channel', JSON.stringify({
              id: data.paint.id,
              positions: redisData.map(JSON.parse)
            }))
          })
      }
    })

  redisClient.lrange('tiles.deferred', 0, -1)
    .then((redisData) => {
      const inDeferred = redisData.map(JSON.parse)
      const i = inDeferred.findIndex(
        (x) => _.isEqual(x.position, data.position))

      if (i > 0) {
        redisClient.multi()
        redisClient.lrem('tiles.deferred', 1, redisData[i])
        redisClient.lpush('tiles.ready', redisData[i])
        redisClient.exec()
        return
      }
    })
    .catch(debug)
})


function handleIncoming() {
  redisClientInc.brpop('tiles.incoming', 0)
    .then((redisData) => {
      const paintData = JSON.parse(redisData[1])
      debug('paint data received (%d points)', paintData.points.length)

      const tilePosToPaint = lines.getLineTilePositions(paintData.points)
      const promises = []

      return redisClientInc.sadd(`tiles.paint.${paintData.id}`,
        tilePosToPaint.map(JSON.stringify))
        .then(() => {
          return redisClientInc.sadd(`tiles.paint-total.${paintData.id}`,
            tilePosToPaint.map(JSON.stringify))
        })
        .then(() => {
          for (const tilePos of tilePosToPaint) {
            redisClientInc.multi()
            redisClientInc.lrange('tiles.processing', 0, -1)
            redisClientInc.lrange('tiles.ready', 0, -1)
            promises.push(redisClientInc.exec()
              .then((redisData) => {
                const inProcessing = redisData[0].map(JSON.parse)
                const inReady = redisData[1].map(JSON.parse)
                const data = JSON.stringify({
                  position: tilePos,
                  paint: paintData
                })

                if (
                  inProcessing.find((x) => _.isEqual(x.position, tilePos)) ||
                  inReady.find((x) => _.isEqual(x.position, tilePos))
                ) {
                  return redisClient.rpush('tiles.deferred', data)
                } else {
                  return redisClient.lpush('tiles.ready', data)
                }
              })
              .catch(debug)
            )
          }

          return Promise.all(promises)
        })
    })
    .catch(debug)
    .then(() => setImmediate(handleIncoming))
}


debug('ready!')
handleIncoming()
